package com.umarfadil.moviedb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.squareup.picasso.Picasso;
import com.umarfadil.moviedb.R;
import com.umarfadil.moviedb.config.api;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Irsyad
 * on 5/20/2017.
 */

public class GridAdapter extends SimpleAdapter{
    LayoutInflater inflater;
    Context context;
    ArrayList<HashMap<String, String>> arrayList;

    public GridAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.context = context;
        this.arrayList = data;
        inflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        ImageView image = (ImageView) view.findViewById(R.id.img_backdrop);

        Picasso.with(context).
                load(api.backdrop_path + arrayList.get(position).get("backdrop_path"))
                .fit().centerCrop()
                .placeholder(R.drawable.image)
                .into(image);

        return view;
    }
}
